import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(13, GPIO.OUT, initial=0)
GPIO.setup(26, GPIO.OUT, initial=0)
GPIO.setup(19, GPIO.OUT, initial=0)
p = GPIO.PWM(13,1000)
p.start(60)
time.sleep(3)
GPIO.output(19, 1)
notdone = 1
count = 0
while notdone and count < 15:
        with open("../output.json","r") as f:
                file = f.read()
                if file != "":
                        notdone = 0
	time.sleep(0.1)
	count = count + 1

GPIO.output(19,0)
p.ChangeFrequency(96)

notdone = 1
while notdone:
        with open("../output.json","r") as f:
                file = f.read()
                if file != "":
                        notdone = 0

GPIO.output(19, 1)
time.sleep(0.5)
GPIO.output(19,0)

with open("dispensed.json","w") as f:
	f.write('"1"')

GPIO.output(26, 1)
time.sleep(0.7)
GPIO.output(26,0)
