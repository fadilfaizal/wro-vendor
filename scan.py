import RPi.GPIO as GPIO
import time
import sys

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(21, GPIO.OUT, initial=0)
GPIO.setup(20, GPIO.OUT, initial=0)
GPIO.setup(16, GPIO.OUT, initial=0)
pid = sys.argv[1]


if pid == "1":
	GPIO.output(21,1)
elif pid == "2":
        GPIO.output(20,1)
elif pid == "3":
	GPIO.output(16,1)
else:
        GPIO.output(21,1)
        GPIO.output(20,1)
        GPIO.output(16,1)

print pid

time.sleep(0.5)
GPIO.output(21,0)
GPIO.output(20,0)
GPIO.output(16,0)
