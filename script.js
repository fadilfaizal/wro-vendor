var carddata = ""; var pincode = ""; var profile; var auto = false; var 
unprocessed = true; var undispensed = true;
$(window).ready(function(){
    step1();
});
function ecl(data){
    if(typeof data === "string")
    if(data.indexOf("Error code: " === 0))
    return false;
    return true;
}
// Insert Card
function step1(){
    var cardcheck = setInterval(function(){
        $.ajax({
            url: "../output.json",
            dataType: "json"
        }).done(function(data){
            carddata = data;
            clearInterval(cardcheck);
            $(".step1").addClass("inserted");
            setTimeout(function(){
                if($("body").attr("step")<2)
                    $("body").attr("step","2");
                step2();
            },1000);
        });
    },1);
}
// Enter Pincode
function step2(){
    if(auto){
        setTimeout(function(){
            $(".pin").val("1234");
            $(".pinsubmit").click();
        },100);
    }
    $("#pin").submit(function(event){
        event.preventDefault();
        pincode = $(".pin").val();
        if(pincode.length !== 4){
            alert("Pincode not 4 digits long.");
        } else if(!$.isNumeric(pincode)){
            alert("Invalid pincode.");
        } else {
            $.ajax({
                url: "../server/api.php",
                dataType: "json",
                data: {
                    user: carddata,
                    pass: pincode,
                    f: "l"
                }
            }).done(function(login){
                profile = login.Profile;
                if(!ecl(login)){
                    $(".pin").css("border-color","indianred");
                    alert("Incorrect pin code.");
                } else {
                    $(".padlock.p1").css("opacity","0");
                    $(".step2").addClass("unlocked");
                    setTimeout(function(){
                        step3();
                    },1500);
                }
            });
        }
    });
}
// Choose Item
function step3(){
    if(auto){
        setTimeout(function(){
            $(".v-item").eq(0).click();
            setTimeout(function(){
                $("#buy").click();
            },100);
        },100);
    }
    if($("body").attr("step")<3)
        $("body").attr("step","3");
    $(".step2").remove();
    $("#name").text(profile.name);
    $("#trsh span").text(profile.credit);
    $(".v-item").click(function(){
        $("#selected.v-item").attr("id","");
        $(this).attr("id","selected");
    });
    $("#buy").click(function(){
        if($("#selected")[0]){
            $(".step3").addClass("picked");
            var item = {
                name: $("#selected .v-name").text(),
                price: $("#selected .v-price").text(),
                rdm: $("#selected .v-rdm").text(),
                pid: $("#selected").attr("pid")
            }
            setTimeout(function(){
                step4(item);
            },500);
        } else
        alert("Pick an item to buy.");
    });
}
// Payment method
function step4(item){
    if($("body").attr("step")<4)
        $("body").attr("step","4");
    if(auto)
        $("#pick").click();
    $(".name").html(item.name);
    $(".price").html(item.price);
    $("#pick").removeAttr("disabled");
    $("#pick").click(function(){
        var cash = $("#choice").prop("checked");
        if(!cash && profile.credit < item.price){
            alert("Not enough trashberries!");
        } else {
            $(".step4").addClass("chose");
            $("#pick").attr("disabled","true");
            step5(item, cash);
            setTimeout(function(){
                if($("body").attr("step","5")<5)
                $("body").attr("step","5");
            },500);
        }
    });
    if(auto)
    $("#pick").click();
}
function step5(item, cash){
    if(unprocessed){
        unprocessed = false;
        $.ajax({
            url: "scan.php",
            dataType: "json",
            data: {
                pid: item.pid
            }
        })
        .done(function(data){
            console.log("Scanned: "+data);
            var undispensed = true;
            if(undispensed){
                undispensed = false;
            }
            var dispensing = setInterval(function(){
                $.ajax({
                    url: "dispensed.json",
                    dataType: "json",
                })
                .done(function(data){
                    if(data != "0"){
                        clearInterval(dispensing);
                        console.log("Dispensing started: "+data);
                        $.ajax({
                            url: "../output.json",
                            dataType: "json"
                        })
                        .done(function(code){
                            console.log("Product code: "+code);
                            $.ajax({
                                url: "../server/api.php",
                                data: {
                                    f: "v",
                                    user: carddata,
                                    pass: pincode,
                                    name: item.name,
                                    pid: item.pid,
                                    code: code,
                                    price: item.price,
                                    rdm: item.rdm,
                                    cash: cash
                                },
                                dataType: "json"
                            })
                            .done(function(result){
                                console.log("Submitted to server: "+result);
                                if(result === "1")
                                step6();
                                else
                                alert("Error while processing.");
                            });
                        });
                    }
                });
            },1000);
        });
    }
}
function step6(){
    $(".step5").addClass("processed");
    setTimeout(function(){
        if($("body").attr("step","6")<6)
            $("body").attr("step","6");
    },500);
    setTimeout(function(){
        reset();
    },5000);
}
function reset(){
    window.location.href = window.location.href;
}
