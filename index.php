<?php file_put_contents("../output.json",""); ?>
<!DOCTYPE html>
<html>
    <head>
        <?php
        echo "<!--";
        var_dump($output);
        echo "-->";
        ?>
        <meta charset="utf-8" />
        <title>Vending Machine</title>
        <link href="style.css" rel="stylesheet" />
        <script src="jquery.js"></script>
        <script src="script.js"></script>
    </head>
    <body step="1">
        <div class="step step1">
            <h1>Insert Card to Continue</h1>
            <img class="card" src="img/card.svg" />
            <div class="cardmessage">
                Insert card to buy item.
            </div>
        </div>
        <div class="step step2">
            <h1>Enter your pincode</h1>
            <img class="padlock p1" src="img/locked.svg" />
            <img class="padlock p2" src="img/unlocked.svg" />
            <form id="pin">
                <input type="password" class="pin" placeholder="••••" maxlength="4" />
                <button type="submit" class="pinsubmit">Submit</button>
            </form>
        </div>
        <div class="step step3">
            <h1>Hello, <span id="name"></span></h1>
            <h3 id="trsh">Trashberries left: <span></span></h3>
            <div class="v-item" pid="1">
                <span class="v-name">Sample 1</span>
                <img class="v-img" src="img/placeholder.png" />
                <span class="v-mny">
                    <img class="v-trsh" src="img/grapes.svg" />/
                    <img class="v-cash" src="img/coins.svg" />:
                    <span class="v-price">5</span>
                </span>
                <span class="rdm">
                    Redeem for
                    <span class="v-rdm">0.5</span> TBs
                </span>
            </div>
            <div class="v-item" pid="2">
                <span class="v-name">Sample 2</span>
                <img class="v-img" src="img/placeholder.png" />
                <span class="v-mny">
                    <img class="v-trsh" src="img/grapes.svg" />/
                    <img class="v-cash" src="img/coins.svg" />:
                    <span class="v-price">3</span>
                </span>
                <span class="rdm">
                    Redeem for
                    <span class="v-rdm">0.2</span> TBs
                </span>
            </div>
            <div class="v-item" pid="3">
                <span class="v-name">Sample 3</span>
                <img class="v-img" src="img/placeholder.png" />
                <span class="v-mny">
                    <img class="v-trsh" src="img/grapes.svg" />/
                    <img class="v-cash" src="img/coins.svg" />:
                    <span class="v-price">2</span>
                </span>
                <span class="rdm">
                    Redeem for
                    <span class="v-rdm">0.1</span> TBs
                </span>
            </div>
            <button id="buy">Buy</button>
        </div>
        <div class="step step4">
            <h1>Method of Payment</h1>
            <h3 id="trsh">Trashberries left: <span></span></h3>
            <h3>Name: <span class="name"></span></h3>
            <h3>Price: $<span class="price"></span></h3>
            <input type="checkbox" checked="true" id="choice" />
            <label for="choice" id="forchoice">
                <span class="choice trashberries"></span>
                <span id="circle"></span>
                <span class="choice cash"></span>
            </label>
            <button id="pick"></button>
        </div>
        <div class="step step5">
            <h1>Processing your request<span class="loader">.</span></h1>
        </div>
        <div class="step step6">
            <h1>All done! Have a nice day!</h1>
        </div>
        <button onclick="reset()" id="reset">Restart</button>
    </body>
</html>
